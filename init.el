;; -*- lexical-binding: t; -*-
;; turbo
(setq gc-cons-threshold 402653184 ; 512mb
      gc-cons-percentage 0.6
      my:init:file-name-handler-alist
      file-name-handler-alist)
(defun my:init::restore-gc ()
  (setq gc-cons-threshold 16777216 ; 16mb
        gc-cons-percentage 0.1
        file-name-handler-alist
        my:init:file-name-handler-alist))
(add-hook 'after-init-hook #'my:init::restore-gc)

;; package
(setq package-archives
      '(("melpa" . "https://melpa.org/packages/")
        ("org" . "https://orgmode.org/elpa/")
        ("gnu" . "https://elpa.gnu.org/packages/")))

;; straight
(setq straight-repository-branch "develop"
      straight-use-package-by-default t
      straight-recipes-emacsmirror-use-mirror t
      straight-recipes-gnu-elpa-use-mirror t
      straight-vc-git-default-clone-depth 1
      straight-check-for-modifications '(check-on-save))
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         (concat "https://raw.githubusercontent.com/"
                 "raxod502/straight.el/develop/install.el")
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; use-package
(straight-use-package 'use-package)
(use-package use-package-ensure-system-package)

;; hush
(use-package hide-mode-line
  :init (progn
          (setq inhibit-splash-screen t
                inhibit-startup-message t
                inhibit-startup-echo-area-message t
                initial-major-mode #'fundamental-mode
                initial-scratch-message nil
                ;; symlinked config
                vc-follow-symlinks t)
          (when (and
                 (fboundp #'scroll-bar-mode)
                 ;; (fboundp #'set-fringe-mode)
                 (fboundp #'tool-bar-mode)
                 (fboundp #'menu-bar-mode))
            (scroll-bar-mode -1)
            ;; (set-fringe-mode 0)
            (tool-bar-mode -1)
            (menu-bar-mode -1))
          (tooltip-mode -1)
          (defalias #'yes-or-no-p #'y-or-n-p))
  :config (hide-mode-line-mode))
(use-package auto-compile
  :init (setq load-prefer-newer t
              auto-compile-display-buffer nil
              auto-compile-mode-line-counter t)
  :config (progn
            ;; (auto-compile-on-load-mode)
            (auto-compile-on-save-mode)))

;; init
(setq user-emacs-directory (file-name-directory load-file-name))
(setq my:init:macs (concat user-emacs-directory "my:macs/")
      my:init:assets (concat user-emacs-directory "my:assets/"))
(add-to-list 'load-path my:init:macs)

;; macs
(require 'my:core)
(require 'my:code)
(require 'my:elisp)
(require 'my:scheme)
(require 'my:shell)
(require 'my:ess)
(require 'my:golang)
;; (require 'my:javascript)
(require 'my:json)
(require 'my:latex)
(require 'my:python)
(require 'my:prose)
(require 'my:org)
(require 'my:markdown)
(require 'my:window)
(require 'my:rice)
(require 'my:misc)
