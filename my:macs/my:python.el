;; -*- lexical-binding: t; -*-
(require 'my:core)
(require 'my:code)

(use-package python
  :defer t
  :init
  (progn
    (setq-default python-indent-offset 4)
    (general-create-definer my:python::general-def
      :wrapping my:core::mmode-general-def
      :prefix-command 'my:python::command
      :prefix-map 'my:python:map
      :keymaps '(python-mode-map inferior-python-mode-map)
      :major-modes t))
  :gfhook
  ('pyton-mode-hook
   '(lsp-deferred ggtags-mode)))
(use-package python-x
  :after python
  :init
  (progn
    (setq python-shell-interpreter "jupyter"
          python-shell-interpreter-args "console --simple-prompt"
          python-shell-prompt-detect-failure-warning nil)
    (add-to-list
     'python-shell-completion-native-disabled-interpreters
     "jupyter"))
  :config
  (progn
    (compdef
     :modes '(python-mode inferior-python-mode)
     :capf '(lsp-completion-at-point
             python-completion-at-point
             comint-completion-at-point
             t))
    (advice-add #'python-shell-switch-to-shell-or-buffer
                :before #'run-python)
    (my:code::ggtags-def
      :keymaps '(python-mode-map
                 inferior-python-mode-map))
    (my:code::dap-def
      :keymaps '(python-mode-map
                 inferior-python-mode-map))
    (general-def
      :keymaps 'python-mode-map
      "M-p" #'python-backward-fold-or-section
      "M-n" #'python-forward-fold-or-section
      [M-return] #'python-shell-send-fold-or-section-and-step)
    (require 'expand-region)
    (er/enable-mode-expansions
     #'python-mode
     #'python-x-mode-expansions))
  :general
  (my:python::general-def
    "," #'python-shell-send-dwim
    "'" #'python-shell-switch-to-shell-or-buffer
    "=" '(lsp-format-buffer :package lsp-mode)

    "s" '(:ignore t :wk "shell")
    "sr" #'python-shell-send-region
    "sR" #'python-shell-restart-process
    "sl" #'python-shell-send-line
    "sL" #'python-shell-send-line-and-step
    "sd" #'python-shell-send-defun
    "sb" #'python-x-shell-send-buffer
    "ss" #'python-shell-send-fold-or-section
    "sS" #'python-shell-send-fold-or-section-and-step

    "h" '(:ignore t :wk "help")
    "hh" #'python-help-for-region-or-symbol
    "he" #'python-eldoc-for-region-or-symbol
    "hp" #'python-shell-print-region-or-symbol
    "hl" '(lsp-describe-thing-at-point :package lsp-mode)
    ;; +++
    ))
(use-package python-pytest
  :after python
  :general
  (my:python::general-def
    "t" '(:ignore t :wk "test")
    "tf" #'python-pytest-file
    "tk" #'python-pytest-file-dwim
    "tt" #'python-pytest-function
    "tm" #'python-pytest-function-dwim
    "tr" #'python-pytest-repeat
    "tp" #'python-pytest-popup))

(use-package conda
  :disabled t
  :defer t
  :init
  (setq conda-anaconda-home "/opt/miniconda3/"
        conda-env-home-directory "~/.conda/")
  :config
  (progn
    (conda-env-initialize-interactive-shells)
    (conda-env-initialize-eshell))
  :general
  (my:python::general-def
    "C" '(:ignore t :wk "conda")
    "Ca" #'conda-env-activate
    "Cd" #'conda-env-deactivate
    "Cl" #'conda-env-list))
(use-package pymacs
  :disabled t
  :straight
  (pymacs :type git
          :host github
          :repo "emacsattic/pymacs"
          :files (("pymacs.el.in" . "pymacs.el")))
  :config (pymacs-load "ropemacs" "rope-"))

(provide 'my:python)
