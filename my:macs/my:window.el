;; -*- lexical-binding: t; -*-

(use-package perspmode
  :disabled t)
(use-package eyebrowse
  :disabled t)
(use-package winum
  :straight
  (winum :type git
         :host github
         :repo "jjzmajic/emacs-winum")
  :after which-key
  :defer nil
  :config (winum-mode)
  :general
  (my:core::general-def
    "`" #'winum-select-window-by-number
    "1" #'winum-select-window-1
    "2" #'winum-select-window-2
    "3" #'winum-select-window-3
    "4" #'winum-select-window-4
    "5" #'winum-select-window-5
    "6" #'winum-select-window-6
    "7" #'winum-select-window-7
    "8" #'winum-select-window-8
    "9" #'winum-select-window-9

    "b" '(:ignore t :wk "buffer")
    "b1" #'winum-buffer-to-window-1
    "b2" #'winum-buffer-to-window-2
    "b3" #'winum-buffer-to-window-3
    "b4" #'winum-buffer-to-window-4
    "b5" #'winum-buffer-to-window-5
    "b6" #'winum-buffer-to-window-6
    "b7" #'winum-buffer-to-window-7
    "b8" #'winum-buffer-to-window-8
    "b9" #'winum-buffer-to-window-9))

(use-package buffer-move
  :defer t
  :general (my:core::general-def
             "b" '(:ignore t :wk "buffer")
             "bh" #'buf-move-left
             "bj" #'buf-move-down
             "bk" #'buf-move-up
             "bl" #'buf-move-right))
(use-package windresize
  :defer t
  :config
  (hercules-def
   :show-funs #'windresize
   :hide-funs '(windresize-exit
                windresize-cancel-and-quit)
   :keymap 'windresize-map
   :flatten t)
  :init
  (my:core::general-def
    "w" '(:ignore t :wk "window")
    "w." #'windresize
    "wh" #'windresize-left
    "wj" #'windresize-down
    "wk" #'windresize-up
    "wl" #'windresize-right
    "wH" #'windresize-select-left
    "wJ" #'windresize-select-down
    "wK" #'windresize-select-up
    "wL" #'windresize-select-right)
  :config
  (general-def
    :keymaps 'windresize-map
    "C-h" nil
    "ESC" #'windresize-cancel-and-quit
    "RET" #'windresize-exit
    "q" #'windresize-exit
    "h" #'windresize-left
    "j" #'windresize-down
    "k" #'windresize-up
    "l" #'windresize-right
    "H" #'windresize-select-left
    "J" #'windresize-select-down
    "K" #'windresize-select-up
    "L" #'windresize-select-right
    "t" #'windresize-toggle-method
    "/" #'windresize-split-window-horizontally
    "-" #'windresize-split-window-vertically
    "?" #'windresize-help
    "SPC" nil))
(use-package winner
  :config (winner-mode +1)
  :general
  (my:core::general-def
    "w" '(:ignore t :wk "window")
    "wu" #'winner-undo ; previous
    "wr" #'winner-redo ; next
    ))
(use-package olivetti
  :defer t
  :init (setq olivetti-body-width 79)
  :general
  (my:core::general-def
    "to" #'olivetti-mode
    "wo" '(:ignore t :wk "olivetti")
    "woo" #'olivetti-mode
    "wo]" #'olivetti-expand
    "wo[" #'olivetti-shrink))

(provide 'my:window)
