;; -*- lexical-binding: t; -*-
(use-package ewal
  :straight
  (ewal :type git
        :files ("ewal.el" "palettes")
        :host gitlab
        :branch "develop"
        :repo "jjzmajic/ewal"
        :depth full)
  :init
  (setq ewal-use-built-in-always-p nil
        ewal-use-built-in-on-failure-p t
        ewal-built-in-palette "sexy-material"))
(use-package ewal-spacemacs-themes
  :after ewal
  :init
  (progn
    (setq spacemacs-theme-underline-parens t
          my:rice:font (font-spec
                        :family "Source Code Pro"
                        :weight 'semi-bold
                        :size 11.0))
    (show-paren-mode +1)
    (global-hl-line-mode)
    (set-frame-font my:rice:font nil t)
    (add-to-list  'default-frame-alist
                  `(font . ,(font-xlfd-name my:rice:font))))
  :config
  (progn
    (load-theme 'ewal-spacemacs-modern t)
    (enable-theme 'ewal-spacemacs-modern)))
(use-package ewal-evil-cursors
  :after ewal-spacemacs-themes ewal
  :config
  (ewal-evil-cursors-get-colors
   :apply t :spaceline t))
(use-package spaceline
  :after (ewal-evil-cursors winum)
  :init (setq powerline-default-separator nil)
  :config (progn
            (spaceline-spacemacs-theme)
            (spaceline-toggle-minor-modes-off)))

(provide 'my:rice)
