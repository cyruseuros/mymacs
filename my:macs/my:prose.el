;; -*- lexical-binding: t; -*-
(require 'my:core)

(use-package flyspell
  :hook ((prog-mode . flyspell-prog-mode)
         (text-mode . flyspell-mode)))
(use-package flyspell-correct-helm
  :commands (flyspell-correct-helm)
  :init (setq flyspell-correct-interface #'flyspell-correct-helm)
  :general (my:core::general-def
             "xc" #'flyspell-correct-wrapper
             "xs" '(:ignore t :wk "spell")
             "xsn" #'flyspell-correct-next
             "xsp" #'flyspell-correct-previous
             ;; point
             "xsp" #'flyspell-correct-at-point
             "xsd" #'ispell-change-dictionary))

(use-package mw-thesaurus
  :defer t
  :general (my:core::general-def
             "xwt" #'mw-thesaurus-lookup-at-point))
(use-package helm-dictionary
  :defer t
  :init (setq helm-dictionary-browser-function #'eww-browse-url
              helm-dictionary-database "/usr/share/dict/de-en.txt")
  :config (add-to-list 'helm-dictionary-online-dicts
                       '(("mw-dictionary" . "https://www.merriam-webster.com/dictionary/%s")))
  :general (my:core::general-def
             "xwd" #'helm-dictionary))

(provide 'my:prose)
